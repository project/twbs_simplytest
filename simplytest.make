api = 2
core = 7.x

includes[] = http://drupalcode.org/project/twbs_simplytest.git/blob_plain/refs/heads/7.x-3.x:/drupal-org-core.make

; Profiles
projects[twbs_simplytest][download][branch] = 7.x-3.x
projects[twbs_simplytest][download][type] = git
projects[twbs_simplytest][download][url] = http://git.drupal.org/project/twbs_simplytest.git
projects[twbs_simplytest][type] = profile

; Modules
projects[twbs_bootstrap][download][branch] = 7.x-3.x
projects[twbs_bootstrap][download][type] = git
projects[twbs_bootstrap][download][url] = http://git.drupal.org/project/twbs_bootstrap.git
projects[twbs_bootstrap][subdir] = contrib
projects[twbs_fontawesome][download][branch] = 7.x-3.x
projects[twbs_fontawesome][download][type] = git
projects[twbs_fontawesome][download][url] = http://git.drupal.org/project/twbs_fontawesome.git
projects[twbs_fontawesome][subdir] = contrib
