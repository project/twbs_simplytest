TWBS simplytest.me
==================

The goal of TWBS simplytest.me was to act as an incubator for Drupal 8
mobile-first experience improvements that can be tested in the field, by
introducing [Bootstrap](http://getbootstrap.com/) integration into
Drupal core.

TWBS simplytest.me is a Drupal distribution which aims to work out
solutions to mobile-first experience problems in the field and apply to
latest development versions of Drupal. Therefore our work started
implementing improvements as modules on Drupal 7 and then our focus
shifted to working on incorporating and enhancing them in Drupal 8 for
core inclusion.

### Distribution for Drupal 7

The Drupal 7 TWBS simplytest.me distribution is a collection of
contributed modules that greatly enhance the mobile-first experience on
top of what Drupal 7 provides. The following features are included:

-   [A responsive toolbar](https://drupal.org/project/twbs_navbar) that
    works well on mobile devices
-   [A contextual link](https://drupal.org/project/twbs_contextual) that
    works well on mobile devices
-   An simple base theme called [TWBS
    Stark](https://drupal.org/project/twbs_stark)

### Author

-   Developed by [Edison Wong](http://drupal.org/user/33940).
-   Sponsored by [PantaRei Design](http://drupal.org/node/1741828).
