api = 2
core = 7.x

; Core
projects[drupal][type] = core
projects[drupal][version] = 7.24

; Patches
projects[drupal][patch][375062] = https://drupal.org/files/drupal-7.x-color_index_out_of_range-375062-52.patch
projects[drupal][patch][728702] = https://drupal.org/files/issues/install-redirect-on-empty-database-728702-36.patch
projects[drupal][patch][737816] = https://drupal.org/files/drupal-7.x-fix_pdoexception_grant_permissions-737816-26-do-not-test.patch
projects[drupal][patch][865536] = https://drupal.org/files/drupal-865536-204.patch
projects[drupal][patch][972536] = https://drupal.org/files/issues/object_conversion_menu_router_build-972536-1.patch
projects[drupal][patch][992540] = https://drupal.org/files/issues/992540-3-reset_flood_limit_on_password_reset-drush.patch
projects[drupal][patch][995156] = https://drupal.org/files/issues/995156-5_portable_taxonomy_permissions.patch
projects[drupal][patch][1470656] = https://drupal.org/files/drupal-1470656-14.patch
projects[drupal][patch][1772316] = https://drupal.org/files/drupal-7.x-allow_profile_change_sys_req-1772316-21.patch
projects[drupal][patch][1815086] = https://drupal.org/files/pdo_oci-d7_22_core_patch-1815086-2.patch
projects[drupal][patch][1815096] = https://drupal.org/files/pdo_oci-d7_driver_patch-1815096-2.patch
projects[drupal][patch][2038567] = https://drupal.org/files/drustack-gitignore-2038567-1.patch
projects[drupal][patch][2106057] = https://drupal.org/files/issues/drupal-7.x-symlinksifownermatch-2106057-5.patch
