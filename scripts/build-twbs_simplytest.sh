#!/bin/bash

MAKEFILE="http://drupalcode.org/project/twbs_simplytest.git/blob_plain/refs/heads/7.x-3.x:/simplytest.make"

drush make \
    --contrib-destination=profiles/twbs_simplytest \
    --concurrency=8 \
    --no-cache \
    --prepare-install \
    $MAKEFILE .
