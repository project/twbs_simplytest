#!/bin/bash

MAKEFILE="http://drupalcode.org/project/twbs_simplytest.git/blob_plain/refs/heads/7.x-3.x:/simplytest.make"

chmod a+w sites/*
rm -rf profiles/twbs_simplytest
drush make \
    --contrib-destination=profiles/twbs_simplytest \
    --concurrency=8 \
    --no-cache \
    $MAKEFILE .
chmod a-w sites/*
