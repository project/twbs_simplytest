#!/bin/bash

PWD=`pwd`
WHOAMI=`whoami`

MAKEFILE="http://drupalcode.org/project/twbs_simplytest.git/blob_plain/refs/heads/7.x-3.x:/simplytest.make"

drush make \
    --contrib-destination=profiles/twbs_simplytest \
    --concurrency=8 \
    --working-copy \
    --no-gitinfofile \
    --no-cache \
    --prepare-install \
    $MAKEFILE .

find $PWD/* -type d -name '.git' | while read LINE; do
    PROJECT=`echo $LINE | sed "s/^.*\/\([^\/]*\)\/\.git/\1/g"`
    cd $LINE/../
    git remote set-url --push origin $WHOAMI@git.drupal.org:project/$PROJECT.git
    git fetch
    git status
    git remote -v
    cd $PWD
done
