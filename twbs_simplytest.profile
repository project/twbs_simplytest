<?php

/**
 * @file
 * Enables modules and site configuration for a TWBS simplytest installation.
 */

/**
 * Implement hook_install_tasks().
 */
function twbs_simplytest_install_tasks($install_state) {
  return array(
    'twbs_simplytest_batch' => array(
      'type' => 'batch',
    ),
  );
}

/**
 * Implements hook_install_tasks_alter().
 */
function twbs_simplytest_install_tasks_alter(&$tasks, $install_state) {
  // Force-enable TWBS modules at any point during the execution of the
  // request.
  $module_path = 'profiles/twbs_simplytest/modules/contrib';
  $modules = array(
    'libraries',
    'twbs',
    'twbs_bootstrap',
    'twbs_fontawesome',
    'twbs_holder',
    'twbs_jquery',
    'twbs_lessphp',
  );

  $module_list = module_list();
  foreach ($modules as $module) {
    $module_list[$module]['filename'] = "${module_path}/${module}/${module}.module";
  }
  module_list(TRUE, FALSE, FALSE, $module_list);
  foreach ($modules as $module) {
    drupal_load('module', $module);
    module_invoke($module, 'init');
  }

  // Force-set a theme at any point during the execution of the request.
  $target_theme = 'twbs_seven';
  if ($GLOBALS['theme'] != $target_theme) {
    unset($GLOBALS['theme']);

    drupal_static_reset();
    $GLOBALS['conf']['maintenance_theme'] = $target_theme;
    _drupal_maintenance_theme();
  }
}

/**
 * Installation step callback.
 */
function twbs_simplytest_batch(&$install_state) {
  $batch['title'] = st('Installing @drupal', array('@drupal' => drupal_install_profile_distribution_name()));
  $batch['operations'][] = array('_twbs_simplytest_configure_avatar', array());
  $batch['operations'][] = array('_twbs_simplytest_configure_modules', array());
  $batch['operations'][] = array('_twbs_simplytest_configure_variables', array());
  $batch['operations'][] = array('_twbs_simplytest_configure_cleanup', array());
  $batch['error_message'] = st('The installation has encountered an error.');
  return $batch;
}

/**
 * Implements hook_update_projects_alter().
 */
function twbs_simplytest_update_projects_alter(&$projects) {
  // Enable update status for the TWBS simplytest profile.
  $modules = system_rebuild_module_data();
  // The module object is shared in the request, so we need to clone it here.
  $twbs_simplytest = clone $modules['twbs_simplytest'];
  $twbs_simplytest->info['hidden'] = FALSE;
  _update_process_info_list($projects, array('twbs_simplytest' => $twbs_simplytest), 'module', TRUE);
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function twbs_simplytest_form_install_configure_form_alter(&$form, $form_state) {
  // Clear drupal message queue for non-errors.
  drupal_get_messages('status', TRUE);
  drupal_get_messages('warning', TRUE);

  // Site information form.
  $form['site_information']['#weight'] = -20;
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
  $form['site_information']['site_mail']['#default_value'] = 'admin@example.com';

  // Administrator account form.
  $form['admin_account']['#weight'] = -19;
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['admin_account']['account']['mail']['#default_value'] = 'admin@example.com';

  // Power user account form.
  $form['webmaster_account'] = array(
    '#type' => 'fieldset',
    '#title' => st('Site power user account'),
    '#collapsible' => FALSE,
  );

  $form['webmaster_account']['#weight'] = -18;
  $form['webmaster_account']['webmaster_account']['#tree'] = TRUE;
  $form['webmaster_account']['webmaster_account']['name'] = array(
    '#title' => st('Username'),
    '#type' => 'textfield',
    '#default_value' => 'webmaster',
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#description' => st('Spaces are allowed; punctuation is not allowed except for periods, hyphens, and underscores.'),
    '#required' => TRUE,
    '#weight' => -10,
    '#attributes' => array('class' => array('username')),
  );

  $form['webmaster_account']['webmaster_account']['mail'] = array(
    '#title' => st('E-mail address'),
    '#type' => 'textfield',
    '#default_value' => 'webmaster@example.com',
    '#maxlength' => EMAIL_MAX_LENGTH,
    '#required' => TRUE,
    '#weight' => -5,
  );

  // Just alter the weight
  $form['server_settings']['#weight'] = -17;
  $form['update_notifications']['#weight'] = -16;
  $form['actions']['#weight'] = -15;

  // Add our own validation
  $form['#validate'][] = '_twbs_simplytest_form_install_configure_form_validate';

  // Add our own submit handler
  $form['#submit'][] = '_twbs_simplytest_form_install_configure_form_submit';
}

/**
 * Validate Power user account
 */
function _twbs_simplytest_form_install_configure_form_validate($form, &$form_state) {
  // Check admin account name.
  if ($error = user_validate_name($form_state['values']['webmaster_account']['name'])) {
    form_error($form['webmaster_account']['webmaster_account']['name'], $error);
  }
  if ($form_state['values']['webmaster_account']['name'] == $form_state['values']['account']['name']) {
    form_error($form['webmaster_account']['webmaster_account']['name'], t('The admin name is not valid.'));
  }

  // Check admin account e-mail address.
  if ($error = user_validate_mail($form_state['values']['webmaster_account']['mail'])) {
    form_error($form['webmaster_account']['webmaster_account']['mail'], $error);
  }
  if ($form_state['values']['webmaster_account']['mail'] == $form_state['values']['account']['mail']) {
    form_error($form['webmaster_account']['webmaster_account']['mail'], t('The admin e-email address is not valid.'));
  }
}

/**
 * Create Power user account and change root password
 */
function _twbs_simplytest_form_install_configure_form_submit($form, &$form_state) {
  // Add the user and associate role here.
  $power_user_role = user_role_load_by_name('power user');
  if (!$power_user_role) {
    $power_user_role = new stdClass();
    $power_user_role->name = 'power user';
    user_role_save($power_user_role);
    $power_user_role = user_role_load_by_name('power user');
  }
  // We keep power user and administrator account password in sync by default.
  $user_data = array(
    'mail' => $form_state['values']['webmaster_account']['mail'],
    'name' => $form_state['values']['webmaster_account']['name'],
    'pass' => $form_state['values']['account']['pass'],
    'init' => $form_state['values']['webmaster_account']['mail'],
    'status' => 1,
    'roles' => array(
      $power_user_role->rid => $power_user_role->name,
    ),
  );
  $account = user_save(new StdClass(), $user_data);
}

/**
 * Set a default user avatar as a managed file object.
 */
function _twbs_simplytest_configure_avatar() {
  $picture = 'user.png';

  $source_dir = 'profiles/twbs_simplytest/files/pictures';
  $source_file = $source_dir . '/' . $picture;

  $destination_dir = file_default_scheme() . '://' . variable_get('user_picture_path', 'pictures');
  $destination_file = $destination_dir . '/' . $picture;

  if (file_prepare_directory($destination_dir, FILE_CREATE_DIRECTORY)) {
    file_save_data($source_file, $destination_file, FILE_EXISTS_REPLACE);
    variable_set('user_picture_default', $destination_file);
  }

  variable_set('user_pictures', 1);
  variable_set('user_picture_path', 'pictures');
  variable_set('user_picture_style', 'thumbnail');
  variable_set('user_picture_dimensions', '1024x1024');
  variable_set('user_picture_file_size', '1024');
}

/**
 * Configure non-backward-compatibile modules.
 */
function _twbs_simplytest_configure_modules() {
  // Enable modules.
  module_enable(array(
    'drustack_article',
    'drustack_blog',
    'drustack_core_custom',
    'drustack_page',
    'twbs_contextual',
    'twbs_navbar',
  ));

  // Revert all features.
  features_revert();
}

/**
 * Configure extra variables.
 */
function _twbs_simplytest_configure_variables() {
  variable_set('block_cache', 0);
  variable_set('cache', 0);
  variable_set('cache_lifetime', 0);
  variable_set('dblog_row_limit', 1000);
  variable_set('error_level', 2);
  variable_set('page_cache_maximum_age', 0);
  variable_set('preprocess_css', 0);
  variable_set('preprocess_js', 0);
  variable_set('update_check_disabled', 1);
}

/**
 * Various actions needed to clean up after the installation.
 */
function _twbs_simplytest_configure_cleanup() {
  // Flush all existing path aliases.
  db_delete('url_alias');

  // Rebuild node access database.
  node_access_rebuild();

  // Rebuild node types.
  node_types_rebuild();

  // Rebuild the menu.
  menu_rebuild();

  // Clear out caches
  drupal_flush_all_caches();

  // Clear out JS and CSS caches.
  drupal_clear_css_cache();
  drupal_clear_js_cache();

  // Revert all features.
  features_revert();

  // Enable git_deploy if clone from GIT.
  $info = drupal_parse_info_file(dirname(__file__) . '/twbs_simplytest.info');
  $version = array_key_exists('version', $info) ? $info['version'] : '7.x-3.x-dev';
  if ($version == '7.x-3.x-dev') {
    module_enable(array('git_deploy'), FALSE);
  }

  // Clear drupal message queue for non-errors.
  drupal_get_messages('status', TRUE);
  drupal_get_messages('warning', TRUE);
}
